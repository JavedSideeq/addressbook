package exercise.test;

import com.google.gwt.junit.client.GWTTestCase;
import exercise.client.core.Factory;
import exercise.client.event.ContactSaveEvent;
import exercise.client.presenter.ContactListPresenter;
import exercise.client.view.ContactListView;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by jawed.sumra on 22/12/13.
 */
public class GwtTestContactListPresenter extends GWTTestCase {

//    @Mock
//    private ContactListView view;
//
//    @Mock
//    private Factory factory;
//
//    @TestSubject
//    private ContactListPresenter classUnderTest = new ContactListPresenter(view, factory);

    // TODO need to move to easy mock
    @Test
    public void testOnContactSave() {

        Factory factory = new FakeClientFactory();
        FakeView display = new FakeView();
        display.createGwtObjects();
        ContactListPresenter classUnderTest = new ContactListPresenter(display, factory);

        FakeContactProxy contactProxy = new FakeContactProxy();
        contactProxy.setForename("Dummy Contact");
        classUnderTest.onContactSave(new ContactSaveEvent(contactProxy));

        assertEquals(display.getData().getList().get(0).getForename(), "Dummy Contact");
    }


    @Override
    public String getModuleName() {
        return "exercise.ExerciseJUnit";
    }

}
