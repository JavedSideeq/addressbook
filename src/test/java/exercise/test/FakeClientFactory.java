package exercise.test;

import com.google.gwt.event.shared.EventBus;
import exercise.client.core.ContactRequestFactory;
import exercise.client.core.Factory;

/**
 * Created by javed.sumra on 02/01/14.
 */
public class FakeClientFactory implements Factory {
    @Override
    public ContactRequestFactory getRequestFactory() {
        return null;
    }

    @Override
    public EventBus getEventBus() {
        return null;
    }
}
