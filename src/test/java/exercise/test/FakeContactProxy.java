package exercise.test;

import com.google.web.bindery.requestfactory.shared.EntityProxyId;
import exercise.client.proxy.ContactProxy;

import java.util.Date;

/**
 * Created by javed.sumra on 02/01/14.
 */
public class FakeContactProxy implements ContactProxy {

    private String forename;

    @Override
    public Integer getVersion() {
        return null;
    }

    @Override
    public void setVersion(Integer version) {

    }

    @Override
    public Date getDob() {
        return null;
    }

    @Override
    public void setDob(Date dob) {

    }

    @Override
    public Long getId() {
        return null;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getForename() {
        return this.forename;
    }

    @Override
    public void setForename(String forename) {
        this.forename = forename;
    }

    @Override
    public String getSurname() {
        return null;
    }

    @Override
    public void setSurname(String surname) {

    }

    @Override
    public String getStreet() {
        return null;
    }

    @Override
    public void setStreet(String street) {

    }

    @Override
    public String getCity() {
        return null;
    }

    @Override
    public void setCity(String city) {

    }

    @Override
    public String getPostalCode() {
        return null;
    }

    @Override
    public void setPostalCode(String postalCode) {

    }

    @Override
    public String getCounty() {
        return null;
    }

    @Override
    public void setCounty(String county) {

    }

    @Override
    public String getCountry() {
        return null;
    }

    @Override
    public void setCountry(String country) {

    }

    @Override
    public EntityProxyId<?> stableId() {
        return null;
    }
}
