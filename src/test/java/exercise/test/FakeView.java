package exercise.test;

import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import exercise.client.presenter.ContactListPresenter;
import exercise.client.proxy.ContactProxy;

import java.util.List;

/**
 * Created by javed.sumra on 02/01/14.
 */
public class FakeView implements ContactListPresenter.Display {

    public void createGwtObjects() {
        System.out.println("hello");
        dataProvider = new ListDataProvider<ContactProxy>();
    }

    @Override
    public HasText getSearchBoxText() {
        return null;
    }

    @Override
    public HasData getCellTable() {
        return null;
    }

    @Override
    public Widget asWidget() {
        return null;
    }

    @Override
    public void setData(List<ContactProxy> data) {

    }

    @Override
    public void setData(ContactProxy data) {
        System.out.println(data.getForename());
        dataProvider.getList().add(data);
    }

    @Override
    public ListDataProvider<ContactProxy> getData() {
        return dataProvider;
    }

    private ListDataProvider<ContactProxy> dataProvider;
}
