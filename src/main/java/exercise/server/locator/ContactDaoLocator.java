package exercise.server.locator;

import com.google.web.bindery.requestfactory.server.RequestFactoryServlet;
import com.google.web.bindery.requestfactory.shared.ServiceLocator;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by jawed.sumra on 18/12/13.
 */
public class ContactDaoLocator implements ServiceLocator {

    /**
     * This class is used by Request Factory to locate the DAO implementation
     * from the web spring context
     * @param clazz
     * @return
     */
    @Override
    public Object getInstance(Class<?> clazz) {
        // TODO should implement log4j logging here
        System.out.println("Inside Dao locator");
        HttpServletRequest request = RequestFactoryServlet.getThreadLocalRequest();
        ServletContext servletContext = request.getSession().getServletContext();
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        return context.getBean( clazz );
    }

}
