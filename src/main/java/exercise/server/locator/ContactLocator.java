package exercise.server.locator;

import com.google.web.bindery.requestfactory.server.RequestFactoryServlet;
import com.google.web.bindery.requestfactory.shared.Locator;
import exercise.domain.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by javed.sumra on 18/12/13.
 *
 * This class is used as a proxy by RF to avoid using implementations in the entity objects
 */
public class ContactLocator extends Locator<Contact, Long> {

    @Override
    public Contact create(final Class<? extends Contact> clazz) {
        // TODO should implement log4j logging here
        System.out.println("Inside ContactLocator - CREATE");
        return new Contact();
    }

    @Override
    public Contact find(Class clazz, Long id) {
        // TODO should implement log4j logging here
        System.out.println("Inside ContactLocator - FIND");
        final HttpServletRequest request = RequestFactoryServlet.getThreadLocalRequest();
        final ServletContext servletContext = request.getSession().getServletContext();
        final ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        EntityManagerFactory entityManagerFactory = context.getBean(EntityManagerFactory.class);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return (Contact) entityManager.find(clazz, id);
    }

    @Override
    public Class<Contact> getDomainType() {
        return Contact.class;
    }

    @Override
    public Long getId(Contact domainObject) {
        return domainObject.getId();
    }

    @Override
    public Class<Long> getIdType() {
        return Long.class;
    }

    @Override
    public Object getVersion(Contact domainObject) {
        return domainObject.getVersion();
    }
}
