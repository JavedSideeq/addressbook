package exercise.server.service;

import exercise.domain.Contact;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by javed.sumra on 31/12/13.
 */
public interface ContactService {

    public Contact create(Contact contact);

    public List<Contact> findAll();

    public Contact findById(Long id);

    public Contact update(Contact contact);


    public List<Contact> findContactBySurnameLike(String string);

}
