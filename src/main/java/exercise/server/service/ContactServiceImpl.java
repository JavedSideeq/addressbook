package exercise.server.service;

import exercise.domain.Contact;
import exercise.server.repository.ContactRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by javed.sumra on 31/12/13.
 */
@Service
public class ContactServiceImpl implements ContactService {

    @Resource
    private ContactRepository contactRepository;

    @Transactional
    @Override
    public Contact create(Contact contact) {
        System.out.println("Inside ContactServiceImpl create");
        return contactRepository.save(contact);
    }

    @Transactional(readOnly=true)
    @Override
    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    @Override
    public Contact findById(Long id) {
        System.out.println("Inside ContactServiceImpl findById");
        return contactRepository.findOne(id);
    }

    @Transactional
    @Override
    public Contact update(Contact contact) {
        System.out.println("UPDATE");
        return contactRepository.saveAndFlush(contact);
    }

    @Transactional(readOnly=true)
    @Override
    public List<Contact> findContactBySurnameLike(String string) {
        return contactRepository.findContactBySurnameLike(string);
    }
}
