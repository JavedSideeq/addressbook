package exercise.server.service;

import com.google.gwt.i18n.client.DateTimeFormat;
import exercise.client.core.ContactRequestContext;
import exercise.client.proxy.ContactProxy;
import exercise.client.util.Constant;
import exercise.domain.Contact;
import exercise.server.repository.ContactRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by javed.sumra on 01/01/14.
 */
@Configuration
public class DummyData {

    @Resource
    private ContactRepository contactRepository;

    @PostConstruct
    public void insertDummyData() {
        String [][] contactsArray = {
                {"James", "Smith", "28-08-1973"},
                {"Elizabeth","Taylor", "22-09-1957"},
                {"Johnathan", "McDonald", "28-08-1981"},
                {"Martin", "Knight", "28-08-1985"},
                {"Julie", "Richards Kendell", "28-08-1980"},
                {"Anna", "Knight", "28-08-1999"},
                {"Anne", "Smith", "28-08-2001"}
        };

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        for (int x = 0; x < contactsArray.length; x++) {
            Contact contact = new Contact();
            contact.setForename(contactsArray[x][0]);
            contact.setSurname(contactsArray[x][1]);
            Date dob = null;
            try {
                dob = sdf.parse(contactsArray[x][2]);
            } catch (ParseException e) {

            }
            contact.setDob(dob);
            contactRepository.save(contact);
        }
    }
}
