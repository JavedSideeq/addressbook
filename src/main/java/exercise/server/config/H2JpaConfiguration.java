
package exercise.server.config;

/**
 * The JPA configuration uses for a H2 database
 */

import org.hibernate.cache.HashtableCacheProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaDialect;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.HashMap;
import java.util.Map;

/** H2 Database Spring Configuration Class. */
@Configuration
public class H2JpaConfiguration {

    @Value("#{dataSource}")
    private javax.sql.DataSource dataSource;


    /**
     * Creates the JPA properties.
     *
     * @return The JPA property map
     */
    @Bean
    public Map<String, Object> jpaProperties() {
        Map<String, Object> props = new HashMap<String, Object>();
        props.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        props.put("hibernate.cache.provider_class", HashtableCacheProvider.class.getName());
        props.put("hbm2ddl.auto", "create");
        props.put("hibernate.archive.autodetection", "class");
        props.put("hibernate.format_sql", "false");
        // Needed for scanning of entity annotations
        props.put("configurationClass", org.hibernate.cfg.Configuration.class.getName());
        return props;
    }

    /**
     * Creates the JPA vendor adapter bean for the required database type.
     *
     * @return The JpaVendorAdapter
     */
    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabase(Database.valueOf("H2"));
        return hibernateJpaVendorAdapter;
    }

    /**
     * Creates the JPA entity manager factory bean. This bean needs a name for the GWT Test Cases as the use the
     * TransactionalRequestFactoryServlet to provide transactions for testing
     *
     * @return The entityManagerFactory
     */
    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean() {
        LocalContainerEntityManagerFactoryBean lef = new LocalContainerEntityManagerFactoryBean();
        lef.setDataSource(this.dataSource);
        lef.setJpaPropertyMap(this.jpaProperties());
        lef.setJpaVendorAdapter(this.jpaVendorAdapter());
        lef.setJpaDialect(this.jpaDialect());
        // Tells the entity which packages should be scanned to find classes with @Entity annotations
        lef.setPackagesToScan("exercise.domain");
        return lef;
    }

    /**
     * Creates the Transaction Manager bean.
     *
     * @return The transaction manager
     */
    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(this.localContainerEntityManagerFactoryBean().getObject());
    }

    /**
     * Creates the JPA dialect
     *
     * @return the JpaDialect
     */
    @Bean
    public JpaDialect jpaDialect() {
        return new HibernateJpaDialect();
    }

}