package exercise.server.repository;

import com.google.web.bindery.requestfactory.shared.Request;
import exercise.client.proxy.ContactProxy;
import exercise.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/** @author imills */

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long>, JpaSpecificationExecutor<Contact> {

    @Query(value = "SELECT * FROM contact WHERE LOWER(surname) LIKE LOWER(CONCAT('%',:string,'%'))", nativeQuery = true)
    List<Contact> findContactBySurnameLike(@Param("string") String string);

}
