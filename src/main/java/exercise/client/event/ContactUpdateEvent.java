package exercise.client.event;

import com.google.gwt.event.shared.GwtEvent;
import exercise.client.handler.ContactEventHandler;
import exercise.client.proxy.ContactProxy;

/**
 * Created by jawed.sumra on 23/12/13.
 */
public class ContactUpdateEvent extends GwtEvent<ContactEventHandler> {

    private final ContactProxy contactProxy;

    public static Type<ContactEventHandler> TYPE = new Type<ContactEventHandler>();

    public ContactUpdateEvent(ContactProxy contactProxy) {
        this.contactProxy = contactProxy;
    }

    @Override
    public Type<ContactEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ContactEventHandler handler) {
        handler.onContactUpdate(this);
    }

    public ContactProxy getContact() {
        return contactProxy;
    }
}
