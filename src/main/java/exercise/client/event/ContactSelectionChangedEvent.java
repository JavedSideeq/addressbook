package exercise.client.event;

import com.google.gwt.event.shared.GwtEvent;
import exercise.client.handler.ContactSelectionChangedHandler;
import exercise.client.proxy.ContactProxy;

/**
 * Created by jawed.sumra on 23/12/13.
 */
public class ContactSelectionChangedEvent extends GwtEvent<ContactSelectionChangedHandler> {

    private final ContactProxy selectedContact;

    public static Type<ContactSelectionChangedHandler> TYPE = new Type<ContactSelectionChangedHandler>();

    public ContactSelectionChangedEvent(ContactProxy selectedContact) {
        this.selectedContact = selectedContact;
    }

    @Override
    public Type<ContactSelectionChangedHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(ContactSelectionChangedHandler handler) {
        handler.onContactSelectionChange(this);
    }

    public ContactProxy getSelectedContact() {
        return selectedContact;
    }
}
