package exercise.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.*;
import exercise.client.core.ClientFactory;
import exercise.client.core.ContactRequestContext;
import exercise.client.core.ContactRequestFactory;
import exercise.client.core.Factory;
import exercise.client.presenter.ContactFormPresenter;
import exercise.client.presenter.ContactListPresenter;
import exercise.client.proxy.ContactProxy;
import exercise.client.util.Constant;
import exercise.client.view.ContactFormView;
import exercise.client.view.ContactListView;



public class Exercise implements EntryPoint {

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {

        Factory clientFactory = new ClientFactory();

        ContactFormView contactFormView = new ContactFormView();
        ContactFormPresenter contactFormPresenter = new ContactFormPresenter(contactFormView, clientFactory);
        contactFormView.setPresenter(contactFormPresenter);
        contactFormPresenter.go(RootPanel.get("leftPane"));

        ContactListView contactListView = new ContactListView();
        ContactListPresenter contactListPresenter = new ContactListPresenter(contactListView, clientFactory);
        contactListView.setPresenter(contactListPresenter);
        contactListPresenter.go(RootPanel.get("rightPane"));

    }
}
