package exercise.client.view;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.view.client.*;
import exercise.client.presenter.ContactListPresenter;
import exercise.client.util.Constant;
import exercise.client.proxy.ContactProxy;
import exercise.client.util.WaterMarkedTextBox;

import java.util.List;

/**
 * Main and currently the only view for the address book application
 */
public class ContactListView extends Composite implements ContactListPresenter.Display {

    private static ContactListViewUiBinder uiBinder = GWT.create(ContactListViewUiBinder.class);

    interface ContactListViewUiBinder extends UiBinder<Widget, ContactListView> {}

    private ContactListPresenter presenter;

    private ListDataProvider<ContactProxy> dataProvider;

    @UiField
    WaterMarkedTextBox textBoxSearch;

    @UiField(provided=true)
    CellTable<ContactProxy> cellTable = new CellTable<ContactProxy>();

    @UiField(provided=true)
    SimplePager pager = new SimplePager();

    @UiHandler("textBoxSearch")
    void onSearchBoxKeyPressUp(KeyUpEvent e) {
        presenter.onSearchBoxKeyPressUp();
    }


    public ContactListView() {

        initWidget(uiBinder.createAndBindUi(this));

        // Create name column.
        TextColumn<ContactProxy> firstNameColumn = new TextColumn<ContactProxy>() {
            @Override
            public String getValue(ContactProxy contact) {
                return contact.getForename();
            }
        };


        // Create address column.
        TextColumn<ContactProxy> lastNameColumn = new TextColumn<ContactProxy>() {
            @Override
            public String getValue(ContactProxy contact) {
                return contact.getSurname();
            }
        };

        // Create id column.
        TextColumn<ContactProxy> dob = new TextColumn<ContactProxy>() {
            @Override
            public String getValue(ContactProxy contact) {
                return DateTimeFormat.getFormat(Constant.DATE_FORMAT).format(contact.getDob());
            }
        };

        cellTable.addColumn(firstNameColumn, "FirstName");
        cellTable.addColumn(lastNameColumn, "LastName");
        cellTable.addColumn(dob, "Date of Birth");
        cellTable.setTableLayoutFixed(true);

        // set the cell-table to the pager widget
        pager.setDisplay(cellTable);

        // set the water-mark text
        textBoxSearch.setWatermark(Constant.LABEL_SEARCH);

        // create a data provider for the table data
        dataProvider = new ListDataProvider<ContactProxy>();
        dataProvider.addDataDisplay(cellTable);
    }

    @Override
    public HasText getSearchBoxText() {
        return textBoxSearch;
    }

    @Override
    public HasData getCellTable() {
        return cellTable;
    }

    @Override
    public ListDataProvider<ContactProxy> getData() {
        return dataProvider;
    }

    public void setPresenter(ContactListPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setData(List<ContactProxy> data) {
        dataProvider.setList(data);
    }

    @Override
    public void setData(ContactProxy data) {
        dataProvider.getList().add(data);
    }


}