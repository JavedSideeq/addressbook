package exercise.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.DateBox;
import exercise.client.util.Constant;
import exercise.client.presenter.ContactFormPresenter;
import exercise.client.proxy.ContactProxy;

import java.util.Date;

/**
 * Created by jawed.sumra on 23/12/13.
 */
public class ContactFormView extends Composite implements ContactFormPresenter.Display {

    private static ContactFormViewUiBinder uiBinder = GWT.create(ContactFormViewUiBinder.class);

    interface ContactFormViewUiBinder extends UiBinder<Widget, ContactFormView> {}

    private ContactFormPresenter presenter;

    @UiField
    TextBox textBoxFirstName;

    @UiField
    TextBox textBoxSurname;

    @UiField
    Button buttonSave;

    @UiField
    Button buttonUpdate;

    @UiField(provided=true)
    DateBox dobPicker = new DateBox();


    @UiHandler("buttonSave")
    void onSaveButtonClicked(ClickEvent e) {
        presenter.onSaveButtonClicked();
    }

    @UiHandler("buttonUpdate")
    void onUpdateButtonClicked(ClickEvent e) {
        presenter.onUpdateButtonClicked();
    }

    public ContactFormView() {
        initWidget(uiBinder.createAndBindUi(this));

        // set the dob format
        dobPicker.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat(Constant.DATE_FORMAT)));

        // set the max length
        textBoxFirstName.setMaxLength(20);

        // set the max length
        textBoxSurname.setMaxLength(20);
    }

    @Override
    public HasText getTextBoxSurname() {
        return textBoxSurname;
    }

    @Override
    public HasText getTextBoxFirstName() {
        return textBoxFirstName;
    }

    @Override
    public HasValue<Date> getDob() {
        return dobPicker;
    }

    public void setPresenter(ContactFormPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setData(ContactProxy data) {
        textBoxFirstName.setText(data.getForename());
        textBoxSurname.setText(data.getSurname());
        dobPicker.setValue(data.getDob());
    }
}
