package exercise.client.util;

/**
 * Created by jawed.sumra on 22/12/13.
 */
public interface Constant {

    //This needs to be localised
    //Text to be displayed on search text box
    public static final String LABEL_SEARCH = "Search by Surname ...";

    //date format used to display dates
    public static final String DATE_FORMAT = "dd-MM-yyyy";

}
