package exercise.client.proxy;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import exercise.domain.Contact;
import exercise.server.locator.ContactLocator;

import java.util.Date;

/**
 * Created by jawed.sumra on 16/12/13.
 */

@ProxyFor(value = Contact.class, locator = ContactLocator.class)
public interface ContactProxy extends EntityProxy {

    public Integer getVersion();
    public void setVersion(Integer version);

    public Date getDob();
    public void setDob(Date dob);

    public Long getId();

    public void setId(Long id);

    public String getForename();

    public void setForename(String forename);

    public String getSurname();

    public void setSurname(String surname);

    public String getStreet();

    public void setStreet(String street);

    public String getCity();

    public void setCity(String city);

    public String getPostalCode();

    public void setPostalCode(String postalCode);

    public String getCounty();

    public void setCounty(String county);

    public String getCountry();

    public void setCountry(String country);

}
