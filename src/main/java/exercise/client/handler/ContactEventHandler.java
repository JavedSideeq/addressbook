package exercise.client.handler;

import com.google.gwt.event.shared.EventHandler;
import exercise.client.event.ContactSaveEvent;
import exercise.client.event.ContactUpdateEvent;

/**
 * Created by jawed.sumra on 23/12/13.
 */
public interface ContactEventHandler extends EventHandler {

    void onContactSave(ContactSaveEvent event);
    void onContactUpdate(ContactUpdateEvent event);

}
