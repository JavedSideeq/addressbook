package exercise.client.handler;

import com.google.gwt.event.shared.EventHandler;
import exercise.client.event.ContactSelectionChangedEvent;

/**
 * Created by jawed.sumra on 23/12/13.
 */
public interface ContactSelectionChangedHandler extends EventHandler {

    void onContactSelectionChange(ContactSelectionChangedEvent event);

}
