package exercise.client.core;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

/**
 * Created by jawed.sumra on 22/12/13.
 * Class to initialise the request context and the event bus
 */
public class ClientFactory implements Factory {

    private final ContactRequestFactory requestFactory;
    private EventBus eventBus;

    public ClientFactory() {
        requestFactory = GWT.create(ContactRequestFactory.class);
        eventBus = new SimpleEventBus();
        requestFactory.initialize(eventBus);
    }

    @Override
    public ContactRequestFactory getRequestFactory() {
        return requestFactory;
    }

    @Override
    public EventBus getEventBus() {
        return eventBus;
    }

}
