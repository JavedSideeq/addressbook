package exercise.client.core;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.Service;

import exercise.client.proxy.ContactProxy;
import exercise.server.locator.ContactDaoLocator;
import exercise.server.service.ContactService;

import java.util.List;


/**
 * Created by jawed.sumra on 17/12/13.
 */

@Service(value = ContactService.class, locator = ContactDaoLocator.class)
public interface ContactRequestContext extends RequestContext {

    /**
     * Find contacts by surname like
     * @param string
     * @return
     */
    Request<List<ContactProxy>> findContactBySurnameLike(String string);

    /**
     * Saves a contact
     * @param contactProxy
     * @return
     */
    Request<ContactProxy> create(ContactProxy contactProxy);


    Request<ContactProxy> update(ContactProxy contactProxy);

    /**
     * Find all contacts
     * @return
     */
    Request<List<ContactProxy>> findAll();



}
