package exercise.client.core;

import com.google.gwt.event.shared.EventBus;

/**
 * Created by javed.sumra on 02/01/14.
 */
public interface Factory {

    ContactRequestFactory getRequestFactory();
    EventBus getEventBus();
}
