package exercise.client.core;


import com.google.web.bindery.requestfactory.shared.RequestFactory;

/**
 * Created by jawed.sumra on 16/12/13.
 */

public interface ContactRequestFactory extends RequestFactory {

    ContactRequestContext contactRequest();
}
