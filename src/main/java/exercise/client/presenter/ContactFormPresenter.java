package exercise.client.presenter;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import exercise.client.core.ClientFactory;
import exercise.client.core.ContactRequestContext;
import exercise.client.core.ContactRequestFactory;
import exercise.client.core.Factory;
import exercise.client.event.*;
import exercise.client.handler.ContactSelectionChangedHandler;
import exercise.client.proxy.ContactProxy;

import java.util.Date;

/**
 * Created by jawed.sumra on 23/12/13.
 */
public class ContactFormPresenter implements ContactSelectionChangedHandler, Presenter {

    private Display display;
    private Factory factory;

    private ContactProxy selectedContact;


    public ContactFormPresenter(Display view, Factory clientFactory) {
        this.display = view;
        this.factory = clientFactory;
    }

    public interface Display {
        //To attach the view to some container we need the view as a widget not only a container
        Widget asWidget();
        HasText getTextBoxFirstName();
        HasText getTextBoxSurname();
        HasValue<Date> getDob();
        void setData(ContactProxy data);
    }

    @Override
    public void go(HasWidgets container) {
        bind();
        container.add(display.asWidget());
    }

    @Override
    public void bind() {
        // add event receivers
        factory.getEventBus().addHandler(ContactSelectionChangedEvent.TYPE, this);
    }

    @Override
    public void onContactSelectionChange(ContactSelectionChangedEvent event) {
        selectedContact = event.getSelectedContact();
        display.setData(event.getSelectedContact());
    }

    public void onSaveButtonClicked() {
        ContactRequestContext request = factory.getRequestFactory().contactRequest();
        ContactProxy contactProxy = request.create(ContactProxy.class);
        contactProxy.setForename(display.getTextBoxFirstName().getText());
        contactProxy.setSurname(display.getTextBoxSurname().getText());
        contactProxy.setDob(display.getDob().getValue());
        request.create(contactProxy).fire(new Receiver<ContactProxy>() {
            @Override
            public void onSuccess(ContactProxy response) {
                factory.getEventBus().fireEvent(new ContactSaveEvent(response));
            }

            @Override
            public void onFailure(ServerFailure error) {
                Window.alert("Oops! Don't worry this should be sorted soon...." + error.getMessage());
            }
        });
    }

    public void onUpdateButtonClicked() {
        final ContactRequestContext request = factory.getRequestFactory().contactRequest();
        final ContactProxy editableContact = request.edit(selectedContact);
        editableContact.setForename(display.getTextBoxFirstName().getText());
        editableContact.setSurname(display.getTextBoxSurname().getText());
        editableContact.setDob(display.getDob().getValue());

        request.update(editableContact).fire(new Receiver<ContactProxy>() {
            @Override
            public void onSuccess(ContactProxy response) {
                selectedContact = response;
                factory.getEventBus().fireEvent(new ContactUpdateEvent(response));
            }

            @Override
            public void onFailure(ServerFailure error) {
                Window.alert("Oops! Don't worry this should be sorted soon...." + error.getMessage());
            }
        });
    }

}
