package exercise.client.presenter;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.*;
import com.google.web.bindery.requestfactory.shared.Receiver;
import exercise.client.core.Factory;
import exercise.client.util.Constant;
import exercise.client.core.ClientFactory;
import exercise.client.core.ContactRequestFactory;
import exercise.client.event.*;
import exercise.client.handler.ContactEventHandler;
import exercise.client.proxy.ContactProxy;

import java.util.List;

/**
 * Created by jawed.sumra on 22/12/13.
 */
public class ContactListPresenter implements Presenter, ContactEventHandler {

    private Display display;
    private Factory factory;

    public ContactListPresenter(Display view, Factory clientFactory) {
        this.display = view;
        this.factory = clientFactory;
    }

    public interface Display {
        HasText getSearchBoxText();
        HasData getCellTable();
        Widget asWidget();
        void setData(List<ContactProxy> data);
        void setData(ContactProxy data);
        ListDataProvider<ContactProxy> getData();
    }

    @Override
    public void go(HasWidgets container) {
        bind();

        // load contacts from db (if any)
        loadAllContacts();

        // finally display
        container.add(display.asWidget());
    }

    @Override
    public void bind() {

        // add event receivers
        factory.getEventBus().addHandler(ContactSaveEvent.TYPE, this);
        factory.getEventBus().addHandler(ContactUpdateEvent.TYPE, this);

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // I am debating with myself if this should go into the view.
        // Personally I'm inclined to leave this in here (i.e. presenter) since SelectionModel is a model and
        // not a widget

        ProvidesKey<ContactProxy> keyProvider = new ProvidesKey<ContactProxy>() {
            public Object getKey(ContactProxy item) {
                // Always do a null check.
                return (item == null) ? null : item.getId();
            }
        };

        final SingleSelectionModel<ContactProxy> singleSelectionModel = new SingleSelectionModel<ContactProxy>(keyProvider);

        // add a single selection model to the cell-table
        display.getCellTable().setSelectionModel(singleSelectionModel);


        singleSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            @Override
            public void onSelectionChange(SelectionChangeEvent event) {
                factory.getEventBus().fireEvent(new ContactSelectionChangedEvent(singleSelectionModel.getSelectedObject()));
            }
        });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
    }

    @Override
    public void onContactSave(ContactSaveEvent event) {
        display.setData(event.getContact());
    }

    //need to change setting of the list so that we set the updated object rather than retrieving from the database
    @Override
    public void onContactUpdate(ContactUpdateEvent event) {
        populateList();
    }

    public void onSearchBoxKeyPressUp() {
        searchBySurnameLike();
    }

    private void populateList() {
        if(display.getSearchBoxText().getText().length() > 0 && !display.getSearchBoxText().getText().equals(Constant.LABEL_SEARCH)) {
            searchBySurnameLike();
        } else {
            loadAllContacts();
        }
    }

    private void searchBySurnameLike() {
        factory.getRequestFactory().contactRequest().findContactBySurnameLike(display.getSearchBoxText().getText()).fire(new Receiver<List<ContactProxy>>() {
            @Override
            public void onSuccess(List<ContactProxy> response) {
                display.setData(response);
            }
        });
    }

    private void loadAllContacts() {
        factory.getRequestFactory().contactRequest().findAll().fire(new Receiver<List<ContactProxy>>() {
            @Override
            public void onSuccess(List<ContactProxy> response) {
                display.setData(response);
            }
        });
    }

}
