package exercise.client.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Created by jawed.sumra on 24/12/13.
 */
public interface Presenter {
    public void go(final HasWidgets container);
    public void bind();
}
